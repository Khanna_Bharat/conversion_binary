#include<iostream>
#define DecimalSize 6LL
#define HexSize 6LL
#define BinarySize (DecimalSize*8LL)

unsigned long long int Binary[BinarySize]={0};
using namespace  std;
class CONVERSION{
public:
    // Hex to binary conversion
    void Hex2Binary(char input_char[HexSize])
    {
        long long int i,j,k;
        k=0;
        for (i = 0; i < DecimalSize; i++)
        {
            for (j = 7; j >= 0; j--)
            {
                if (((input_char[i] >> j) & 1) != 0)
                {
                    Binary[k] = 1;
                }
                else
                {
                    Binary[k] = 0;
                }
                k++;
            }
        }
    }
};
int main()
{
    CONVERSION conversion;
    char input_char[HexSize] = {'h','a','c','k','e','r'};
    conversion.Hex2Binary(input_char);
    cout<<"Binary value is ";
    for(int i = 0; i <BinarySize; i++)
    {
        cout<<Binary[i];
    }
    cout<<endl;
}

