#include <iostream>
#include<vector>

using namespace std;

vector<bool> b;
int MAX_COUNT = 0;

void Decimal2Binary(int d)
{
	while(d)
	{
        b.push_back(d%2);
        d = d/2;
	}
    for(int i = 0 ; i < b.size() ;i++)
    {
        int m = 0;
        while( (b[i]) && (i < b.size()))
        {
            m += 1;
            i++;
        }    
        if(m > MAX_COUNT)
            MAX_COUNT = m;
    }
    cout<<MAX_COUNT<<endl;
}

int main(){
    int n;
    cin >> n;
    Decimal2Binary(n);
    return 0;
}

