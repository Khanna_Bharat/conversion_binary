#include<iostream>
#define DecimalSize 2
#define HexSize 2
#define BinarySize (DecimalSize*8)
using namespace  std;
class CONVERSION{
public:
// Hex to binary conversion
void Hex2Binary(int Hex[HexSize])
{
int Binary[BinarySize]={0};
int i,j,k;
k=0;
    for (i = 0; i < DecimalSize; i++)
    	{
    		for (j = 7; j >= 0; j--)		
    		{
    			if (((Hex[i] >> j) & 1) != 0)
    			{
    				Binary[k] = 1;
    			}
    			else
    			{
    				Binary[k] = 0;
    			}
    			k++;
    		}
    	}
cout<<"Binary value is ";
for(i = 0; i <BinarySize; i++)
{
cout<<Binary[i];
}
cout<<endl;
}
};
int main()
{
CONVERSION conversion;
int Hex[HexSize] = {0xAA,0x55};
conversion.Hex2Binary(Hex);
}
